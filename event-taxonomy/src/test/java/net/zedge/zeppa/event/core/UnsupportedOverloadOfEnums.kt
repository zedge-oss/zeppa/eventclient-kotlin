package net.zedge.zeppa.event.core

import org.apache.thrift.TEnum

class UnsupportedOverloadOfEnums : MapPropertiesSetter<UnsupportedOverloadOfEnums>(Scope.Event) {
    enum class ThriftEnum(private val value: Int) : TEnum {SECOND(1), THIRD(2);
        override fun getValue() = value
    }

    enum class SecondThriftEnum(private val value: Int) : TEnum {SECOND(1), THIRD(3);
        override fun getValue() = value
    }

    fun generalEnum(value: ThriftEnum) = set("general_enum", value, ThriftEnum::class.java)
    fun generalEnum(value: SecondThriftEnum) = set("general_enum", value, SecondThriftEnum::class.java)
}
