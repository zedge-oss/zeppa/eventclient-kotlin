package net.zedge.zeppa.event.core

class ChangingEnumToString : MapPropertiesSetter<ChangingEnumToString>(Scope.Event) {
    fun generalEnum(stringValue: String) = set("general_enum", stringValue)
}
