package net.zedge.zeppa.event.taxonomy

import net.zedge.zeppa.event.core.MapPropertiesSetter
import net.zedge.zeppa.event.core.MapPropertiesSetter.Companion.CLASS_TEMPLATE
import net.zedge.zeppa.event.core.MapPropertiesSetter.Companion.ENUM_TYPE
import net.zedge.zeppa.event.core.NotEventProperty
import net.zedge.zeppa.event.core.Scope
import net.zedge.zeppa.event.taxonomy.Taxonomy.Lifecycle.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.time.Instant

interface EnumValuePool {
    fun add(value: Int): Boolean
    fun getNextValue(constant: Any?, enum: Class<*>): Int
}

open class EnumValues : EnumValuePool {
    val values = mutableSetOf<Int>()
    override fun add(value: Int) = values.add(value)
    override fun getNextValue(constant: Any?, enum: Class<*>): Int = getLowestAvailableValue()

    protected fun getLowestAvailableValue(higherOrEqualTo: Int = 1): Int {
        var nextValue = higherOrEqualTo
        while (values.contains(nextValue)) nextValue += 1
        return nextValue
    }
}

open class Taxonomy constructor(
        private val appId: String,
        private val defaultLifecycle: Lifecycle = PreProduction,
        private val timestamp: Instant = Instant.now(),
        private val enumValuePoolFactory: () -> EnumValuePool = { EnumValues() }) {
    private val enums: MutableMap<String, EnumConstants> = mutableMapOf()
    private val properties: MutableMap<String, JSONObject> = mutableMapOf()

    enum class Lifecycle {
        Production, ProductionEndOfLife, PreProduction, Void;

        fun isChangeAllowed(nextLifecycle: Lifecycle) = when (this) {
            ProductionEndOfLife -> nextLifecycle == Production
            Production -> nextLifecycle == ProductionEndOfLife
            PreProduction -> nextLifecycle == Void || nextLifecycle == Production
            Void -> nextLifecycle != ProductionEndOfLife
        }

        fun endLife() = when (this) {
            Production -> ProductionEndOfLife
            PreProduction -> Void
            else -> this
        }

        fun isAlive() = when (this) {
            ProductionEndOfLife -> false
            else -> true
        }
    }

    companion object {
        private const val NOT_IN_USE = "notInUse"
        private const val LAST_MODIFIED = "last_modified"

        private const val TYPE = "type"
        private const val SCOPE = "scope"
        private const val LIFECYCLE = "lifecycle"

        private const val NAME_COLLISION_SUGGESTION = "Consider choosing a different property name"
    }

    constructor(
            taxonomy: JSONObject,
            releaseType: Lifecycle = PreProduction,
            timestamp: Instant = Instant.now(),
            enumValuePoolFactory: () -> EnumValuePool = { EnumValues() }
    ) : this(taxonomy.getString("appid"), releaseType, timestamp, enumValuePoolFactory) {
        if (taxonomy.has("enums")) taxonomy.getJSONArray("enums")
                .map { it as JSONObject }
                .associateTo(enums) {
                    it.getString("name") to EnumConstants(it.getJSONArray("fields"), enumValuePoolFactory)
                }
        if (taxonomy.has("properties")) taxonomy.getJSONArray("properties")
                .map { it as JSONObject }
                .associateTo(properties) { it.getString("name") to it.put(NOT_IN_USE, true) }
    }

    inner class EnumConstants(constants: JSONArray = JSONArray(), enumValuePoolFactory: () -> EnumValuePool) {
        private val constants: MutableMap<String, JSONObject> = mutableMapOf()

        private val values = enumValuePoolFactory()

        init {
            constants.map { it as JSONObject }.forEach { constant ->
                this.constants[constant.getString("name")] = constant.put(NOT_IN_USE, true)
                values.add(constant.getInt("value"))
            }
        }

        fun update(enum: Class<*>) {
            enum.enumConstants.forEach { constant ->
                val name = constant.toString()
                if (constants.containsKey(name)) {
                    constants[name]!!.markAsInUse()
                } else {
                    val value = values.getNextValue(constant, enum)
                    values.add(value)
                    constants[name] = newValue()
                            .put("name", name)
                            .put("value", value)
                }
            }
        }

        fun toJSONArray(): JSONArray {
            val result = JSONArray()
            constants.values.forEach { result.put(it) }
            return result
        }

        fun deleteIfUnused() {
            constants.values.removeIf { it.finalizeLifecycle() == Void }
        }
    }

    fun update(vararg instances: MapPropertiesSetter<*>) {
        addAllProperties(*instances)
        deleteUnusedValues()
    }

    fun deleteUnusedValues() {
        properties.values.removeIf { it.finalizeLifecycle() == Void }
        enums.values.forEach { it.deleteIfUnused() }
    }

    internal fun JSONObject.finalizeLifecycle(): Lifecycle {
        if (has(NOT_IN_USE)) {
            remove(NOT_IN_USE)
            return endLife()
        }
        return getLifecycle()
    }

    private fun JSONObject.endLife(): Lifecycle {
        val lifecycle = getLifecycle()
        if (lifecycle.isAlive() && !isSacred()) {
            val result = lifecycle.endLife()
            put(LIFECYCLE, result)
            put(LAST_MODIFIED, timestamp)
            return result
        }
        return lifecycle
    }

    private fun JSONObject.isSacred() = if (has(SCOPE)) getScope().isSacred() else false

    private fun JSONObject.getScope() = getEnum(Scope::class.java, SCOPE)

    fun addEnum(eventEnum: Class<*>, extend: Class<*>? = null) {
        extend?.let { addEnum(extend) }
        val baseType = extend ?: eventEnum
        enums.getOrPut(baseType.simpleName) { EnumConstants(enumValuePoolFactory = enumValuePoolFactory) }.update(eventEnum)
    }

    fun newValue(): JSONObject = JSONObject()
            .put(LIFECYCLE, defaultLifecycle)
            .put(LAST_MODIFIED, timestamp)

    fun addAllProperties(vararg instances: MapPropertiesSetter<*>) {
        instances.forEach { addAllProperties(it) }
    }

    fun addAllProperties(instance: MapPropertiesSetter<*>) {
        instance.javaClass.declaredMethods.forEach { method ->
            if (!method.isSynthetic && !method.isAnnotationPresent(NotEventProperty::class.java)) {
                addProperties(method, instance)
            }
        }
    }

    fun addProperties(method: Method, instance: MapPropertiesSetter<*>) {
        instance.recorder = propertyUpdater
        method.isAccessible = true
        try {
            method.invoke(instance, *instantiateParameters(method))
        } catch (e: InvocationTargetException) {
            throw e.targetException
        }
    }

    private val propertyUpdater = object : MapPropertiesSetter.PropertyRecorder {
        override fun record(key: String, typeTemplate: String, scope: Scope, enum: Class<*>?, extend: Class<*>?) {
            var type = typeTemplate
            enum?.let {
                val baseType = extend ?: enum
                type = typeTemplate.replace(CLASS_TEMPLATE, baseType.simpleName)
                addEnum(enum, extend)
            }
            addProperty(key, scope, type)
        }
    }

    private fun addProperty(key: String, scope: Scope, type: String) {
        if (properties.containsKey(key)) {
            properties[key]!!.updateProperty(scope, type)
        } else {
            properties[key] = newValue()
                    .put(TYPE, type)
                    .put("name", key)
                    .put(SCOPE, scope)
        }
    }

    private fun instantiateParameters(method: Method) = method.parameterTypes.map {
        instantiateParameter(it)
    }.toTypedArray()

    open fun instantiateParameter(parameter: Class<*>) = when {
        parameter.isEnum -> parameter.enumConstants[0]
        parameter.isArray -> when (parameter.componentType) {
            Boolean::class.java -> booleanArrayOf(true)
            Byte::class.java -> byteArrayOf(Byte.MAX_VALUE)
            Short::class.java -> shortArrayOf(Short.MAX_VALUE)
            Int::class.java -> intArrayOf(Int.MAX_VALUE)
            Long::class.java -> longArrayOf(Long.MAX_VALUE)
            Float::class.java -> floatArrayOf(Float.MAX_VALUE)
            Double::class.java -> doubleArrayOf(Double.MAX_VALUE)
            Boolean::class.javaObjectType -> arrayOf(true)
            Byte::class.javaObjectType -> arrayOf(Byte.MAX_VALUE)
            Short::class.javaObjectType -> arrayOf(Short.MAX_VALUE)
            Int::class.javaObjectType -> arrayOf(Int.MAX_VALUE)
            Long::class.javaObjectType -> arrayOf(Long.MAX_VALUE)
            Float::class.javaObjectType -> arrayOf(Float.MAX_VALUE)
            Double::class.javaObjectType -> arrayOf(Double.MAX_VALUE)
            String::class.javaObjectType, String::class.java -> arrayOf("String")
            else -> throw NotImplementedError("Please add support for array" +
                    " of ${parameter.componentType.name} above this line")
        }
        else -> when (parameter) {
            Boolean::class.javaObjectType, Boolean::class.java -> true
            Byte::class.javaObjectType, Byte::class.java -> 1.toByte()
            Short::class.javaObjectType, Short::class.java -> 1.toShort()
            Int::class.javaObjectType, Int::class.java -> 1
            Long::class.javaObjectType, Long::class.java -> 1L
            Float::class.javaObjectType, Float::class.java -> Float.MAX_VALUE
            Double::class.javaObjectType, Double::class.java -> Double.MAX_VALUE
            List::class.javaObjectType, List::class.java,
            Collection::class.javaObjectType, Collection::class.java -> {
                emptyList<String?>()
            }
            Map::class.javaObjectType, Map::class.java -> {
                emptyMap<String?, String?>()
            }
            else -> try {
                parameter.getDeclaredConstructor().newInstance()
            } catch (e: Throwable) {
                throw NotImplementedError("Unable to instantiate ${parameter.name}." +
                        " Move all calls to set(), append(), and increase() to helper" +
                        " methods and annotate the function with @NotEventProperty \n$e")
            }
        }
    }

    private fun JSONObject.updateProperty(scope: Scope, type: String) {
        val currentScope = getEnum(Scope::class.java, SCOPE)
        assertValidScopeChange(currentScope, scope)
        put(SCOPE, scope)
        assertValidTypeChange(type)
        put(TYPE, type)
        if (scope != currentScope) put(LAST_MODIFIED, timestamp)
        markAsInUse()
    }

    private fun JSONObject.assertValidScopeChange(currentScope: Scope, scope: Scope) {
        if (getLifecycle() != ProductionEndOfLife && !currentScope.isScopeAllowed(scope)) {
            val expectedPropertyType = currentScope.getFullName()
            val propertyType = scope.getFullName()
            val name = get("name")
            throw RuntimeException("Property \"$name\" cannot be $propertyType" +
                    " because it is, or has been, $expectedPropertyType. $NAME_COLLISION_SUGGESTION.")
        }
    }

    private fun JSONObject.assertValidTypeChange(type: String) {
        val currentType = getString(TYPE)
        if (currentType.contains(ENUM_TYPE) && currentType != type && type != "String") throw RuntimeException(
                "Cannot change property \"${get("name")}\" from $currentType to $type." +
                        " Extend the enum type of $currentType instead by providing it to your set() method!")
    }

    private fun JSONObject.getLifecycle() = getEnum(Lifecycle::class.java, LIFECYCLE)

    private fun JSONObject.markAsInUse() {
        remove(NOT_IN_USE)
        val currentLifecycle = getLifecycle()
        val nextLifecycle =
                if (currentLifecycle == ProductionEndOfLife) Production
                else defaultLifecycle
        if (currentLifecycle.isChangeAllowed(nextLifecycle)) {
            put(LIFECYCLE, nextLifecycle)
            put(LAST_MODIFIED, timestamp)
        }
    }

    fun toJson(): JSONObject {
        val taxonomy = JSONObject()
        taxonomy.put("appid", appId)
        taxonomy.put("enums", enumsAsJSONArray())
        taxonomy.put("properties", propertiesAsJSONArray())
        return taxonomy
    }

    private fun enumsAsJSONArray(): JSONArray {
        val enumArray = JSONArray()
        enums.forEach {
            enumArray.put(JSONObject().put("name", it.key).put("fields", it.value.toJSONArray()))
        }
        return enumArray
    }

    private fun propertiesAsJSONArray(): JSONArray {
        val propertyArray = JSONArray()
        properties.forEach { propertyArray.put(it.value) }
        return propertyArray
    }

}
