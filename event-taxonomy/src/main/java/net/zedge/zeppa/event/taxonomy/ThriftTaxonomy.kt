package net.zedge.zeppa.event.taxonomy

import org.apache.thrift.TEnum
import java.time.Instant

class ThriftEnumValues : EnumValues() {
    override fun getNextValue(constant: Any?, enum: Class<*>): Int =
            if (enum.interfaces.contains(TEnum::class.java)) {
                getLowestAvailableValue((constant as TEnum).value)
            } else {
                super.getNextValue(constant, enum)
            }
}


class ThriftTaxonomy(
        appId: String,
        defaultLifecycle: Lifecycle = Lifecycle.PreProduction,
        timestamp: Instant = Instant.now()
) : Taxonomy(appId, defaultLifecycle, timestamp, enumValuePoolFactory = { ThriftEnumValues() }) {
    override fun instantiateParameter(parameter: Class<*>) =
            if (parameter.isAssignableFrom(TEnum::class.java)) parameter.getConstructor(Int::class.java).newInstance(1)
            else super.instantiateParameter(parameter)
}
