package net.zedge.event.schema

import net.zedge.zeppa.event.core.MapPropertiesSetter
import net.zedge.zeppa.event.core.NotEventProperty
import net.zedge.zeppa.event.core.Properties
import net.zedge.zeppa.event.core.Scope

/**
 * Properties of a User
 *
 * Methods of this class are called with reflection during build target "buildTaxonomy"
 * and makes up the event logging taxonomy of user properties.
 *
 * The key defines a property, and value defines its type whenever a methods calls set(key, value).
 *
 * These properties becomes user properties in amplitude, and columns in the zedge events database.
 */
class UserProperties : MapPropertiesSetter<UserProperties>(Scope.User) {
    companion object {
        @NotEventProperty @JvmStatic fun of() = UserProperties()
        @NotEventProperty @JvmStatic fun of(context: Properties) = of().context(context)
    }

    /* See EventProperties to get examples of how to create a property */

    fun userString(value: String) = set("user_string", value)
    fun userFloat(value: Float) = set("user_float", value)
    fun userArray(value: Array<String>) = set("user_string_array", value)

    fun incrementInt(increment: Int) = increment("increment_int", increment)
    fun incrementLong(increment: Long) = increment("increment_long", increment)
    fun incrementFloat(increment: Float) = increment("increment_float", increment)
    fun incrementDouble(increment: Double) = increment("increment_double", increment)
    fun appendString(increment: String) = append("append_string", increment)
    fun appendKotlinArray(value: Array<String>) = append("string_array", value)
    fun appendKotlinArray(value: Array<Boolean>) = append("boolean_array", value.toBooleanArray())
    fun appendKotlinArray(value: Array<Byte>) = append("byte_array", value.toByteArray())
    fun appendKotlinArray(value: Array<Short>) = append("short_array", value.toShortArray())
    fun appendKotlinArray(value: Array<Int>) = append("int_array", value.toIntArray())
    fun appendKotlinArray(value: Array<Long>) = append("long_array", value.toLongArray())
    fun appendKotlinArray(value: Array<Float>) = append("float_array", value.toFloatArray())
    fun appendKotlinArray(value: Array<Double>) = append("double_array", value.toDoubleArray())
    fun appendNativeArray(value: BooleanArray) = append("boolean_array", value)
    fun appendNativeArray(value: ByteArray) = append("byte_array", value)
    fun appendNativeArray(value: ShortArray) = append("short_array", value)
    fun appendNativeArray(value: IntArray) = append("int_array", value)
    fun appendNativeArray(value: LongArray) = append("long_array", value)
    fun appendNativeArray(value: FloatArray) = append("float_array", value)
    fun appendNativeArray(value: DoubleArray) = append("double_array", value)
}
