package net.zedge.event.schema

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

internal class EventPropertiesTest {
    @Test
    fun testDeserializing() {
        val eventProperties = EventProperties.of().stringValue("value").apply {
            addAll(UserProperties.of()
                .userString("user")
                .userFloat(3.0f)
                .incrementDouble(4.0)
                .appendKotlinArray(arrayOf("test", "value")))
        }
        val deserialized = EventProperties.deserialize(EventProperties.serialize(eventProperties))
        assertThat(deserialized).isEqualTo(eventProperties)
    }

    @Test
    fun testDeserializeNullDoesNotThrow() {
        EventProperties.deserialize(null)
    }
}
