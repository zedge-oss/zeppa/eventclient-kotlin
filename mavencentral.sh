#!/usr/bin/env bash
set -e
JAVA_HOME=$JAVA_11_HOME ./gradlew clean javadocJar
./gradlew test assemble
./gradlew uploadArchives
