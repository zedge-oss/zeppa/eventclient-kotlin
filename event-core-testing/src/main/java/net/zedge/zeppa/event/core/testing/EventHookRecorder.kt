package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.EventLoggerHook
import net.zedge.zeppa.event.core.LoggableEvent
import net.zedge.zeppa.event.core.Properties

class EventHookRecorder<EventProperties, UserProperties>: EventLoggerHook {
    val events = mutableListOf<EventProperties>()
    val userProperties = mutableListOf<UserProperties>()
    @Suppress("UNCHECKED_CAST")
    override fun onEvent(event: LoggableEvent) {
        events.add(event as EventProperties)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onIdentifyUser(properties: Properties) {
        userProperties.add(properties as UserProperties)
    }
}
