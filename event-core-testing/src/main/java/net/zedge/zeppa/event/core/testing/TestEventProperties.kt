package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.*

/**
 * Properties of an Event
 *
 * Methods of this class are called with reflection during build target "buildTaxonomy"
 * and makes up the event logging taxonomy of event properties.
 *
 * The key defines a property, and value defines its type whenever a methods calls set(key, value).
 *
 * These properties becomes event properties in amplitude, and columns in the zedge events database.
*/
class TestEventProperties: MapEventProperties<TestEventProperties>(::of) {
    companion object {
        @JvmStatic fun of() = TestEventProperties()
        @JvmStatic fun of(event: TestEvent) = event.with()
        @JvmStatic fun of(context: Properties) = of().context(context)
    }
    fun log(logger: EventLogger) = logger.log(this)

    internal fun event(event: TestEvent) = set(EVENT_KEY, event, TestEvent::class.java, Scope.Envelope)

    fun passiveEvent(passive: Boolean?) = set("passive_event", passive)
    fun activeEvent(active: Boolean?) = passiveEvent(active?.let{!it})
    fun enumValue(enum: ThriftEnum?) = set("enum", enum)
    fun count(count: Int) = apply {setCount(count)}
    fun clientTimestamp(timestamp: Long) = apply { setClientTimestamp(timestamp) }
}
