package net.zedge.zeppa.event.core

/**
 * Mapping event names, property names and property values to new names.
 */
open class Mapper(
        propertyMappings: Map<Properties, String> = mapOf(),
        propertyValueMappings: Map<Properties, String> = mapOf(),
        private val keyMapping: ((String) -> String)? = null,
        private val enumMapping: ((String) -> String)? = null,
        val logger: EventLogger
) : EventLogger, EventMapping {

    private val propertyMappings: Map<String, String> = propertyMappings.flatMap { mapping ->
        mapping.key.toFlatJson().keys().asSequence().toList().map { it to mapping.value }
    }.toMap()

    private val valueMappings: Map<String, Map<out Any, String>>
            = mutableMapOf<String, MutableMap<Any, String>>().apply {
        propertyValueMappings.forEach { mapping ->
            val properties = mapping.key.toFlatMap()
            properties.forEach { (key, value) ->
                if (value is String || value.javaClass.isEnum) {
                    if (!containsKey(key)) put(key, mutableMapOf())
                    getValue(key)[value] = mapping.value
                }
            }
        }
    }

    companion object {
        @JvmStatic
        val BOOK_TITLE: (String) -> String = { it.uppercaseEachWord(" ") }

        private fun String.uppercaseEachWord(separator: String) = splitToSequence("_").joinToString(separator) {
            when {
                it.isNotEmpty() -> it.substring(0, 1).uppercase() + it.substring(1).lowercase()
                else -> it
            }
        }

        @JvmStatic
        val UPPER_CAMEL_CASE: (String) -> String = { it.uppercaseEachWord("") }

        @JvmStatic
        val LOWER_CAMEL_CASE: (String) -> String = { propertyName ->
            propertyName.splitToSequence("_").mapIndexed { index, it ->
                when {
                    index == 0 -> it.lowercase()
                    it.isNotEmpty() -> it.substring(0,1).uppercase() + it.substring(1).lowercase()
                    else -> it
                }
            }.joinToString("")
        }

        @JvmStatic
        val LOWERCASE: (String) -> String = { it.lowercase() }
    }

    override fun log(event: LoggableEvent) {
        logger.log(event.translate(this))
    }

    override fun getKeyMapping(key: String): String? {
        return propertyMappings[key] ?: keyMapping?.let { map ->
            map(key)
        }
    }

    override fun getValueMapping(key: String, value: Any) : Any? {
        return valueMappings[key]?.get(value) ?: if (value.javaClass.isEnum) enumMapping?.let {
            map -> map(value.toString())
        } else null
    }

    override fun identifyUser(properties: Properties) {
        logger.identifyUser(properties.translate(this))
    }

}
