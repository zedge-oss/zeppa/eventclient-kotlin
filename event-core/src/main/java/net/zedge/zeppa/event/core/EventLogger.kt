package net.zedge.zeppa.event.core

interface EventLogger {
    fun log(loggable: EventRepresentation) = log(loggable.toEvent())
    fun log(event: LoggableEvent)
    fun identifyUser(properties: Properties)
}

