package net.zedge.zeppa.event.core

import com.amplitude.api.Identify
import org.json.JSONObject

interface Properties {
    fun toProperties(): JSONObject
    fun toZedgeJson(): JSONObject
    fun toFlatJson(): JSONObject
    fun toFlatMap(): Map<String, Any>
    fun identifyUser(): Identify
    fun removeProperty(property: String)
    fun copy(): Properties
    fun translate(mapping: EventMapping): Properties
    fun intersect(other: Properties)
    fun addAll(valuesToOverwrite: Properties)
}
