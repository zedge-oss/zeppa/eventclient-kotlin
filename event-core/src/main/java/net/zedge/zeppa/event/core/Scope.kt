package net.zedge.zeppa.event.core

enum class Scope {
    Event, User, Envelope, Internal;
    fun getFullName(): String = when (this) {
        Event -> "an event property"
        Envelope -> "an envelope property"
        User -> "a user property"
        Internal -> "an internal property"
    }
    /**
     * We allow changing between internal, envelope and user scope
     */
    fun isScopeAllowed(nextScope: Scope) = when (this) {
        Event -> nextScope == Event
        Envelope -> nextScope != Event
        User -> nextScope != Event
        Internal -> nextScope != Event
    }

    fun isSacred() = when (this) {
        Envelope, Internal -> true
        else -> false
    }
}
