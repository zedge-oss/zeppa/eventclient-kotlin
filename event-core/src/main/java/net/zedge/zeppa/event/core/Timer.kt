package net.zedge.zeppa.event.core

/**
 * Timer for setting client_timestamp
*/
interface Timer {
    fun elapsedMillisecondsFromReferencePoint(): Long
    fun fallbackEpochMillisecondsNow(): Long
}