package net.zedge.zeppa.event.core

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface ZedgeLoggerRetrofitService {

    @Headers("Content-Type: application/json")
    @POST("events")
    fun log(@Body events: String, @Query("build") build: String): Call<String>
}
