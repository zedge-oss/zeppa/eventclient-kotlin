package net.zedge.zeppa.event.core

/**
 * Properties of an Event
 *
 * Methods of this class are called with reflection during build target "buildTaxonomy"
 * and makes up the event logging taxonomy of event properties.
 *
 * The key defines a property, and value defines its type whenever a methods calls set(key, value).
 *
 * These properties becomes event properties in amplitude, and columns in the zedge events database.
 */
open class MapEventProperties<out T : LoggableEvent>(protected val create: () -> T)
    : MapPropertiesSetter<T>(Scope.Event), LoggableEvent {
    companion object {
        const val EVENT_KEY = "event"
    }

    @NotEventProperty
    override fun copy() = cast(create()).context(this)

    @Suppress("UNCHECKED_CAST")
    @NotEventProperty
    private fun cast(t: T) = t as MapEventProperties<T>

    @NotEventProperty
    override fun getEventString() = envelope.get(EVENT_KEY).toString()

    @NotEventProperty
    override fun toProperties() = super.toProperties().apply { remove(EVENT_KEY) }

    @NotEventProperty
    override fun translate(mapping: EventMapping) = cast(copy()).apply {
        envelope.map(mapping)
        properties.map(mapping)
        increments.map(mapping)
    }

    @NotEventProperty
    fun with() = copy()

    @NotEventProperty
    fun with(context: Properties?) = cast(copy()).context(context)

    @NotEventProperty
    fun with(event: EventRepresentation) = with(event.toEvent())

    override fun setClientTimestamp(timestamp: Long) {
        set("client_timestamp", timestamp, Scope.Internal)
    }

    override fun setDedupeKey(key: Short) {
        set("event_dedupe_key", key, Scope.Internal)
    }

    override fun setCount(count: Int) {
        set("count", count)
    }
}
