package net.zedge.zeppa.event.core

class LoggingEventGate(private val event: LoggableEvent, queueSizeLimit: Int = 1024) :
        EventGate(queueSizeLimit, { count, logger ->
            logger.log(event.copy().apply { setCount(count) })
        }) {

    constructor(event: LoggableEvent, logger: EventLogger, queueSizeLimit: Int = 1024) : this(event, queueSizeLimit) {
        this.logger = logger
    }
}