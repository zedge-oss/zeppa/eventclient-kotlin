package net.zedge.zeppa.event.core

import com.amplitude.api.Identify
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.ConcurrentHashMap


open class MapProperties : Properties {
    protected var envelope = ConcurrentHashMap<String, Any>()
    protected var increments = ConcurrentHashMap<String, Any>()
    protected var properties = ConcurrentHashMap<String, Any>()

    @NotEventProperty
    override fun copy(): Properties = MapProperties().apply { addAll(this@MapProperties) }

    @NotEventProperty
    override fun toProperties() = toFlatJson()

    @NotEventProperty
    @Synchronized
    override fun addAll(valuesToOverwrite: Properties) {
        if (valuesToOverwrite is MapProperties) {
            envelope.putAll(valuesToOverwrite.envelope)
            properties.putAll(valuesToOverwrite.properties)
            increments.putAll(valuesToOverwrite.increments)
        } else {
            throw NotImplementedError("Unsupported properties type " + valuesToOverwrite.javaClass.name)
        }
    }

    @NotEventProperty
    override fun translate(mapping: EventMapping) = (copy() as MapProperties).apply {
        envelope.map(mapping)
        properties.map(mapping)
        increments.map(mapping)
    }

    protected fun ConcurrentHashMap<String, Any>.map(mapping: EventMapping) = keys().toList().forEach { key ->
        val value = get(key)!!
        mapping.getValueMapping(key, value)?.let { put(key, it) }
        mapping.getKeyMapping(key)?.let { put(it, remove(key)!!) }
    }

    @NotEventProperty
    override fun toFlatJson(): JSONObject {
        val result = properties.toJsonObject()
        serialize(envelope, result)
        return result
    }

    private fun Map<String, Any>.toJsonObject() = JSONObject().apply {
        serialize(this@toJsonObject, this)
    }

    @NotEventProperty
    override fun toFlatMap(): Map<String, Any> = properties.toMutableMap().apply {
            envelope.forEach { (k, v) -> put(k, v) }
    }

    @NotEventProperty
    private fun serialize(source: Map<String, Any>, destination: JSONObject) {
        source.entries.forEach { (key, value) ->
            when {
                value.javaClass.isEnum -> destination.put(key, value.toString())
                else -> destination.put(key, JSONObject.wrap(value))
            }
        }
    }

    @NotEventProperty
    override fun toZedgeJson(): JSONObject {
        val result = convertZedgeValues(envelope).toJsonObject()
        val zedgeProperties = convertZedgeValues(increments)
        zedgeProperties.putAll(convertZedgeValues(properties))
        result.put("properties", zedgeProperties.toJsonObject())
        return result
    }

    @NotEventProperty
    protected fun convertZedgeValues(properties: ConcurrentHashMap<String, Any>) = properties.entries.map { (key, value) ->
        when {
            value is Boolean -> key to mapZedgeBoolean(value)
            value.javaClass.isEnum -> key to value.toString()
            value is Array<*> && value.size > 0 && value[0] is Boolean -> key to value.map { mapZedgeBoolean(it as Boolean) }.toByteArray()
            value is BooleanArray -> key to value.map { mapZedgeBoolean(it as Boolean) }.toByteArray()
            else -> key to value
        }
    }.toMap(ConcurrentHashMap<String, Any>())

    private fun mapZedgeBoolean(value: Boolean): Byte = if (value) 1 else 0

    @NotEventProperty
    @Suppress("UNCHECKED_CAST")
    override fun identifyUser(): Identify = toProperties().toIdentify().apply {
        increments.entries.forEach { (key, value) ->
            when (value) {
                is Int, Short, Byte -> add(key, value as Int)
                is Double -> add(key, value)
                is Float -> add(key, value)
                is Long -> add(key, value)
                is Number -> add(key, value.toDouble())
                is IntArray -> append(key, value)
                is BooleanArray -> append(key, value)
                is FloatArray -> append(key, value)
                is DoubleArray -> append(key, value)
                is Array<*> -> append(key, value as Array<out String>)
                else -> append(key, value.toString())
            }
        }
    }

    @NotEventProperty
    @Suppress("UNCHECKED_CAST")
    private fun JSONObject.toIdentify(): Identify {
        val identification = Identify()
        keys().forEach { key ->
            when (val value = get(key)) {
                is Boolean -> identification.set(key, value)
                is Int, Short, Byte -> identification.set(key, value as Int)
                is Double -> identification.set(key, value)
                is Float -> identification.set(key, value)
                is Long -> identification.set(key, value)
                is Number -> identification.set(key, value.toDouble())
                is IntArray -> identification.set(key, value)
                is BooleanArray -> identification.set(key, value)
                is FloatArray -> identification.set(key, value)
                is DoubleArray -> identification.set(key, value)
                is Array<*> -> identification.set(key, value as Array<out String>)
                else -> {
                    val stringValue = value.toString()
                    if (stringValue.isEmpty()) {
                        identification.unset(key)
                    } else {
                        identification.set(key, stringValue)
                    }
                }
            }
        }
        return identification
    }

    @NotEventProperty
    override fun equals(other: Any?): Boolean {
        if (other is MapProperties) {
            return toZedgeJson().toString().equals(other.toZedgeJson().toString())
        }
        return false
    }

    @NotEventProperty
    override fun toString(): String {
        return toZedgeJson().toString()
    }

    @NotEventProperty
    fun serialize() = JSONObject()
        .put("envelope", envelope.toJsonObject())
        .put("properties", properties.toJsonObject())
        .put("increments", increments.toJsonObject())
        .toString()


    @NotEventProperty
    @Synchronized
    fun deserialize(serialized: String?) {
        serialized?.let {
            val deserialized = JSONObject(it)
            envelope.putAll(toMap(deserialized.getJSONObject("envelope")))
            properties.putAll(toMap(deserialized.getJSONObject("properties")))
            increments.putAll(toMap(deserialized.getJSONObject("increments")))
        }
    }

    @NotEventProperty
    private fun toMap(serialized: JSONObject): Map<String, Any> {
        val map: MutableMap<String, Any> = HashMap()
        val keys = serialized.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            var value = serialized[key]
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            map[key] = value
        }
        return map
    }

    @NotEventProperty
    open fun toList(array: JSONArray): List<Any> {
        val list: MutableList<Any> = ArrayList()
        for (i in 0 until array.length()) {
            var value = array[i]
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            list.add(value)
        }
        return list
    }

    @NotEventProperty
    override fun removeProperty(property: String) {
        properties.remove(property)
        envelope.remove(property)
        increments.remove(property)
    }

    @NotEventProperty
    @Synchronized
    override fun intersect(other: Properties) {
        if (other is MapProperties) {
            properties.intersect(other.properties)
            envelope.intersect(other.envelope)
            increments.intersect(other.increments)

        } else {
            throw NotImplementedError("Unsupported intersection with property type: " + other.javaClass.name)
        }
    }

    private fun <K, V> MutableMap<K, V>.intersect(other: Map<K, V>) {
        this.keys.toList().intersect(other.keys.toList()).forEach {
            if (get(it) != other.get(it)) this.remove(it)
        }
    }
}

