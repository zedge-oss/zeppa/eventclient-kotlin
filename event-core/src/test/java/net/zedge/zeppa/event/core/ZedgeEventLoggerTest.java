package net.zedge.zeppa.event.core;

import com.google.gson.Gson;

import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes;
import net.zedge.zeppa.event.core.testing.TestEvent;
import net.zedge.zeppa.event.core.testing.TestEventProperties;
import net.zedge.zeppa.event.core.testing.ThriftEnum;

import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes;
import net.zedge.zeppa.event.core.testing.TestEvent;
import net.zedge.zeppa.event.core.testing.TestEventProperties;
import net.zedge.zeppa.event.core.testing.ThriftEnum;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.awaitility.Awaitility.await;

@RunWith(JUnit4.class)
public class ZedgeEventLoggerTest {

    private MockWebServer server = new MockWebServer();
    private EventLogger logger;
    private ZedgeEventLogger zedge;

    @Before
    public void setUp() {
        logger = createLogger();
    }

    private EventLogger createLogger() {
        HttpUrl url = server.url("/api/logsink/");
        zedge = new ZedgeEventLogger(url.toString(), new OkHttpClient.Builder().build(), "release");
        return new UserPropertiesEventDecorator(zedge, new SupportedPropertyTypes(Scope.User));
    }

    @Test
    public void testSingleEvent() throws Exception {
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        SerializedEvent firstEvent = assertStartAppEvent();

        assertThat(firstEvent.properties.size()).isEqualTo(2);
    }

    private SerializedEvent assertStartAppEvent() throws InterruptedException {
        RecordedRequest request = server.takeRequest();
        assertThat(request.getHeader("Content-Type")).isEqualTo("application/json");
        SerializedEvent event = getSerializedEvents(request).events.get(0);
        assertThat(event.event).isEqualTo(TestEvent.EVENT_WITH_THRIFT_ENUM.name());
        assertThat(event.properties.get("thrift_enum")).isEqualTo(ThriftEnum.FIRST.name());
        return event;
    }

    static SerializedEvents getSerializedEvents(RecordedRequest request) {
        Charset charset = Charset.forName("UTF-8");
        String body = request.getBody().readString(charset);
        return new Gson().fromJson(body, SerializedEvents.class);
    }

    static class SerializedEvents {
        public List<SerializedEvent> events;
    }

    static class SerializedEvent {
        public String event;
        public String envelope;
        public double envelope_boolean;
        public Map<String, Object> properties;
    }

    @Test
    public void testEventWithProperties() throws Exception {
        TestEvent.EVENT_WITH_THRIFT_ENUM.with(SupportedPropertyTypes.of().shortValue((short)10).stringValue("theError")).log(logger);
        assertEventWithProperties();
    }

    private void assertEventWithProperties() throws InterruptedException {
        SerializedEvent firstEvent = assertStartAppEvent();
        assertThat(firstEvent.properties.size()).isEqualTo(4);
        assertThat(firstEvent.properties.get("string")).isEqualTo("theError");
        assertThat(firstEvent.properties.get("short")).isEqualTo(10.);
    }

    @Test
    public void testAlternativeWriting() throws Exception {
        logger.log(TestEvent.EVENT_WITH_THRIFT_ENUM.with(SupportedPropertyTypes.of().shortValue((short)10).stringValue("theError")));
        assertEventWithProperties();

        TestEventProperties hitsContext = TestEventProperties.of(SupportedPropertyTypes.of().shortValue((short) 10));
        SupportedPropertyTypes sectionContext = SupportedPropertyTypes.of().stringValue("theError");
        TestEvent.EVENT_WITH_THRIFT_ENUM.with(hitsContext).with(sectionContext).log(logger);
        assertEventWithProperties();

        hitsContext.with(TestEvent.EVENT_WITH_THRIFT_ENUM).with(sectionContext).log(logger);
        assertEventWithProperties();
    }

    @Test
    public void testAlternativeWritingWithoutProperties() throws Exception {
        logger.log(TestEvent.EVENT_WITH_THRIFT_ENUM);
        SerializedEvent firstEvent = assertStartAppEvent();
        assertThat(firstEvent.properties.size()).isEqualTo(2);
        assertThat(firstEvent.properties.get("thrift_enum")).isEqualTo(ThriftEnum.FIRST.name());
    }

    @Test
    public void testEventWithEnum() throws Exception {
        TestEvent.START_APP.with().enumValue(ThriftEnum.SECOND).log(logger);
        RecordedRequest request = server.takeRequest();
        SerializedEvent firstEvent = getSerializedEvents(request).events.get(0);
        assertThat(firstEvent.event).isEqualTo(TestEvent.START_APP.name());
        assertThat(firstEvent.properties.get("enum")).isEqualTo(ThriftEnum.SECOND.name());
    }

    @Test
    public void testExponentialBackoffRetriesOnServerError() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(500));
        server.enqueue(new MockResponse().setResponseCode(599));
        server.enqueue(new MockResponse().setResponseCode(550));
        server.enqueue(new MockResponse().setBody("{}"));
        zedge.setMillisecondsBeforeRetry(200);
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        await().atLeast(100, TimeUnit.MILLISECONDS).until(new RequestCountLargerThan(2));
        await().atLeast(300, TimeUnit.MILLISECONDS).until(new RequestCountLargerThan(3));
        await().atLeast(700, TimeUnit.MILLISECONDS).until(new RequestCountLargerThan(4));
        assertThat(server.takeRequest()).isNotNull();
        assertThat(server.takeRequest()).isNotNull();
        assertThat(server.takeRequest()).isNotNull();
        assertThat(server.takeRequest()).isNotNull();
        assertThat(server.takeRequest(100, TimeUnit.MILLISECONDS)).isNull();
    }

    class RequestCountLargerThan implements Callable<Boolean> {
        private int requestCount;

        public RequestCountLargerThan(int requestCount) {
            this.requestCount = requestCount;
        }

        @Override
        public Boolean call() throws Exception {
            return server.getRequestCount() >= requestCount;
        }
    }

    @Test
    public void testExponentialBackoffStopsRetryingOnBadRequests() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(400));
        zedge.setMillisecondsBeforeRetry(200);
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        assertThat(server.takeRequest(100, TimeUnit.MILLISECONDS)).isNotNull();
        assertThat(server.takeRequest(500, TimeUnit.MILLISECONDS)).isNull();
    }

    @Test
    public void testExponentialBackoffStopsRetryingOnSuccess() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(200));
        zedge.setMillisecondsBeforeRetry(200);
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        assertThat(server.takeRequest(100, TimeUnit.MILLISECONDS)).isNotNull();
        assertThat(server.takeRequest(500, TimeUnit.MILLISECONDS)).isNull();
    }

    @Test
    public void testSetUserProperties() throws Exception {
        EventLogger logger = createLogger();
        logger.identifyUser(new SupportedPropertyTypes(Scope.User)
                .envelopeProperty("0000121aa0b5458cc06ef9d71445b69bda496105")
                .longValue(10L));
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        SerializedEvent event = assertStartAppEvent();

        assertThat(event.envelope).isEqualTo("0000121aa0b5458cc06ef9d71445b69bda496105");
        assertThat(event.properties.size()).isEqualTo(3);
        assertThat(event.properties.get("long")).isEqualTo(10.);
    }

    @Test
    public void testSetUserProperties_DoesNotAlterEventProperties() {
        EventLogger logger = createLogger();
        logger.identifyUser(new SupportedPropertyTypes(Scope.User).stringValue("abcd"));
        TestEventProperties event = TestEvent.EVENT_WITH_THRIFT_ENUM.with().enumValue(ThriftEnum.SECOND);
        logger.log(event);
        assertThat(event).isEqualTo(TestEvent.EVENT_WITH_THRIFT_ENUM.with().enumValue(ThriftEnum.SECOND));
    }

    @Test
    public void testSerializeBooleanAndLong() throws Exception {
        EventLogger logger = createLogger();
        logger.identifyUser(new SupportedPropertyTypes(Scope.User).longValue(10L));
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        String body = server.takeRequest().getBody().readString(Charset.forName("UTF-8"));
        JSONObject properties = new JSONObject(body)
                .getJSONArray("events")
                .getJSONObject(0)
                .getJSONObject("properties");
        assertThat(properties.get("long")).isEqualTo(10);
    }

    @Test
    public void testLoggingNullValues() throws Exception {
        TestEvent.EVENT_WITH_THRIFT_ENUM.with(SupportedPropertyTypes.of()
                .stringValue(null)
                .nullableLong(null)
                .nullableThriftEnum(null)).log(logger);
        SerializedEvent event = assertStartAppEvent();
        assertThat(event.properties.size()).isEqualTo(2);
    }

    @Test
    public void testEventDedupeKey() throws Exception {
        TestEvent.EVENT_WITH_THRIFT_ENUM.log(logger);
        SerializedEvent event = assertStartAppEvent();
        assertThat(event.properties.size()).isEqualTo(2);
        double key = (double)event.properties.get("event_dedupe_key");
        assertThat(key).isBetween(Double.valueOf(Short.MIN_VALUE), Double.valueOf(Short.MAX_VALUE));
    }
}
