package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.JavaTimer
import net.zedge.zeppa.event.core.testing.TestEvent
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test
import java.time.Instant.now

class ClientTimestampDecoratorIntegrationTest {
    private val recorder = EventLoggerRecorder()
    private var logger = ClientTimestampDecorator(
            logger = recorder,
            queueSizeLimit = 1024,
            eventOnOverflow = TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.toEvent(),
            timer = JavaTimer(),
            onSynchronize = {
                recorder.log(TestEvent.SYNCHRONIZE_EVENT_CLOCK.with().apply { setClientTimestamp(it) })
            })

    @Test
    fun testClockAdjustmentForPastEvents() {
        val beforeLog = now().toEpochMilli()
        logger.log(TestEvent.START_APP)
        val afterLog = now().toEpochMilli()
        assertThat(recorder.events).isEmpty()

        val waitingTime = 200L
        Thread.sleep(waitingTime)

        val beforeSync = now().toEpochMilli()
        logger.synchronizeTime(now().toEpochMilli())
        val afterSync = now().toEpochMilli()

        val error = afterSync - beforeSync
        assertThat(error).isLessThan(waitingTime)

        assertThat(recorder.events.size).isEqualTo(2)
        assertThat(recorder.events[0].getEventString()).isEqualTo(TestEvent.SYNCHRONIZE_EVENT_CLOCK.name)
        assertThat(recorder.events[0].toProperties().getLong("client_timestamp"))
                .isBetween(beforeSync, afterSync)
        assertThat(recorder.events[1].getEventString()).isEqualTo(TestEvent.START_APP.name)
        assertThat(recorder.events[1].toProperties().getLong("client_timestamp"))
                .isBetween(beforeLog - error, afterLog + error)
    }

}