package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import net.zedge.zeppa.event.core.testing.ThriftEnum
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class AmplitudeEventLoggerTest {
    @Test
    fun testAmplitudeProperties() {
        val event = TestEvent.START_APP.with().enumValue(ThriftEnum.FIRST)
        event.addAll(SupportedPropertyTypes(Scope.User)
                .envelopeProperty("outer")
                .stringValue("id").longValue(10L))
        val json = event.toProperties()
        assertThat(json["enum"]).isEqualTo("FIRST")
        assertThat(json["string"]).isEqualTo("id")
        assertThat(json["envelope"]).isEqualTo("outer")
        assertThat(json.has("event")).isFalse()
        assertThat(json["long"]).isEqualTo(10L)
    }
}
