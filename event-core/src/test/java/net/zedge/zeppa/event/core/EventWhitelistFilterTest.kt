package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import net.zedge.zeppa.event.core.testing.ThriftEnum
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventWhitelistFilterTest {
    val recorder = EventLoggerRecorder()
    val logger = EventWhitelistFilter(setOf(TestEvent.START_APP.name), recorder)

    @Test
    fun testWhitelistingOfEvents() {
        val event = TestEvent.START_APP.with().enumValue(ThriftEnum.SECOND)
        event.log(logger)
        TestEvent.RESUME_APP.with().enumValue(ThriftEnum.SECOND).log(logger)
        assertThat(recorder.events[0]).isEqualTo(event)
        assertThat(recorder.events.size).isEqualTo(1)
    }

    @Test
    fun testIdentifyUser() {
        val userProperties = SupportedPropertyTypes(Scope.User).longValue(1)
        logger.identifyUser(userProperties)
        assertThat(recorder.userProperties[0]).isEqualTo(userProperties)
    }
}
