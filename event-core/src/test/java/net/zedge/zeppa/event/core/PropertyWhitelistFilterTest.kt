package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import net.zedge.zeppa.event.core.testing.ThriftEnum
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class PropertyWhitelistFilterTest {
    val recorder = EventLoggerRecorder()
    val logger = PropertyWhitelistFilter(setOf("thrift_enum", "string"), recorder)
    @Test
    fun testBlacklistingProperties() {
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of()
                .thriftEnum(ThriftEnum.FIRST)
                .shortValue(2)
                .intValue(5))
        val copy = event.copy()
        event.log(logger)
        assertThat(recorder.events.size).isEqualTo(1)
        val properties = recorder.events[0].toProperties()

        assertThat(properties.has("short")).isFalse()
        assertThat(properties.has("int")).isFalse()
        assertThat(properties["thrift_enum"]).isEqualTo("FIRST")
        assertThat(properties.keySet().size).isEqualTo(1)
        assertThat(event).isEqualTo(copy)
    }

    @Test
    fun testIdentifyUser() {
        val userProperties = SupportedPropertyTypes().intValue(1).stringValue("theId").booleanValue(true)
        val copy = userProperties.copy()
        logger.identifyUser(userProperties)
        assertThat(recorder.userProperties.size).isEqualTo(1)
        assertThat(recorder.userProperties[0]).isEqualTo(SupportedPropertyTypes().stringValue("theId"))
        assertThat(userProperties).isEqualTo(copy)
    }

    @Test
    fun testEventIsAlwaysPresent() {
        TestEvent.START_APP.log(logger)
        assertThat(recorder.events[0].getEventString()).isEqualTo(TestEvent.START_APP.name)
    }
}
