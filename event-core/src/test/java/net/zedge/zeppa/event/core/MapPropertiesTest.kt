package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.GeneralEnum
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.ThriftEnum
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class MapPropertiesTest {
    @Test
    fun testToZedgeJson_translatesEnvelopeBooleanToNumber() {
        val envelopeProperties = SupportedPropertyTypes()
            .booleanValue(true)
            .envelopeBoolean(true)
            .envelopeBooleanArray(booleanArrayOf(true, false))
        val zedgeJson = envelopeProperties.toZedgeJson()
        assertThat(zedgeJson.getJSONObject("properties").getInt("boolean")).isEqualTo(1)
        assertThat(zedgeJson.getInt("envelope_boolean")).isEqualTo(1)
        assertThat(zedgeJson.getJSONArray("envelope_boolean_array").getInt(0)).isEqualTo(1)
        assertThat(zedgeJson.getJSONArray("envelope_boolean_array").getInt(1)).isEqualTo(0)
    }

    @Test
    fun testToZedgeJson_translateEnumArraysToStringArray() {
        val enumArray = SupportedPropertyTypes()
            .enumArray(listOf(ThriftEnum.FIRST, ThriftEnum.SECOND))
        val zedgeJson = enumArray.toZedgeJson()
        val properties = zedgeJson.getJSONObject("properties")
        assertThat(properties.getJSONArray("enum_array").getString(0)).isEqualTo("FIRST")
        assertThat(properties.getJSONArray("enum_array").getString(1)).isEqualTo("SECOND")
    }

    @Test
    fun testToZedgeJson_includesIncrements() {
        val envelopeProperties = SupportedPropertyTypes(Scope.User)
            .booleanValue(true)
            .incrementDouble(3.3)
            .appendString("test")
            .appendKotlinArray(arrayOf<Int>(1, 2, 3))
        val zedgeJson = envelopeProperties.toZedgeJson()

        val properties = zedgeJson.getJSONObject("properties")
        assertThat(properties.getInt("boolean")).isEqualTo(1)
        assertThat(properties.getDouble("increment_double")).isEqualTo(3.3)
        assertThat(properties.getString("append_string")).isEqualTo("test")
        assertThat(properties.getJSONArray("int_array").getInt(0)).isEqualTo(1)
        assertThat(properties.getJSONArray("int_array").getInt(2)).isEqualTo(3)
    }

    @Test
    fun testToFlatJson() {
        val envelopeProperties = SupportedPropertyTypes()
            .booleanValue(true)
            .envelopeBoolean(true)
            .envelopeBooleanArray(booleanArrayOf(true, false))
        val zedgeJson = envelopeProperties.toFlatJson()

        assertThat(zedgeJson.get("boolean")).isEqualTo(true)
        assertThat(zedgeJson.get("envelope_boolean")).isEqualTo(true)
        assertThat(zedgeJson.getJSONArray("envelope_boolean_array").get(0)).isEqualTo(true)
        assertThat(zedgeJson.getJSONArray("envelope_boolean_array").get(1)).isEqualTo(false)
    }

    @Test
    fun testDeserializing() {
        val properties = SupportedPropertyTypes()
            .appendKotlinArray(arrayOf(3.5))
            .incrementDouble(2.0)
            .appendString("test")
            .stringValue("value")
            .doubleValue(3.0)
            .envelopeBoolean(true)
            .envelopeProperty("e")
            .enumArray(listOf(ThriftEnum.SECOND, ThriftEnum.FIRST))
            .generalEnum(GeneralEnum.THIRD)
        val deserialized = SupportedPropertyTypes().apply { deserialize(properties.serialize()) }
        assertThat(deserialized).isEqualTo(properties)
    }
}
