package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.Mapper.Companion.BOOK_TITLE
import net.zedge.zeppa.event.core.testing.*
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventMapperTest {
    val recorder = EventLoggerRecorder()

    @Test
    fun testValueTranslations() {
        val eventTranslations = mapOf(TestEvent.START_APP as Any to "Translated_Start_App")
        val propertyValueTranslations = mapOf<Properties, String>(
                SupportedPropertyTypes.of().thriftEnum(ThriftEnum.FIRST) to "Correct_Enum",
                SupportedPropertyTypes.of().stringValue("ID") to "Old_ID")
        val logger = EventMapper(eventTranslations, mapOf(), propertyValueTranslations, logger = recorder)
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().thriftEnum(ThriftEnum.FIRST).stringValue("ID"))
        val copy = event.copy()
        event.log(logger)
        assertThat(recorder.events.size).isEqualTo(1)
        assertThat(recorder.events[0].getEventString()).isEqualTo("Translated_Start_App")
        val properties = recorder.events[0].toProperties()

        assertThat(properties["thrift_enum"]).isEqualTo("Correct_Enum")
        assertThat(properties["string"]).isEqualTo("Old_ID")

        assertThat(event).isEqualTo(copy)
    }

    @Test
    fun testEventTranslations() {
        val propertyTranslations = mapOf<Properties, String>(
                SupportedPropertyTypes.of().thriftEnum(ThriftEnum.ZERO) to "content")
        val logger = EventMapper(propertyMappings = propertyTranslations, logger = recorder)
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().thriftEnum(ThriftEnum.FIRST))
        event.log(logger)

        assertThat(recorder.events.size).isEqualTo(1)
        val properties = recorder.events[0].toProperties()
        assertThat(recorder.events[0].getEvent()).isEqualTo(TestEvent.START_APP)
        assertThat(properties.has("enum")).isFalse()
        assertThat(properties["content"]).isEqualTo("FIRST")
    }

    private fun LoggableEvent.getEvent(): TestEvent = TestEvent.valueOf(getEventString())

    @Test
    fun testLoggingOfUserProperties() {
        val userProperties = SupportedPropertyTypes(Scope.User).longValue(1)
        EventMapper(logger = recorder).identifyUser(userProperties)
        assertThat(recorder.userProperties[0]).isEqualTo(userProperties)
    }

    @Test
    fun testBothEventAndValueTranslations() {
        val eventTranslations = mapOf<Any, String>(TestEvent.START_APP to "Start_App")
        val propertyTranslations = mapOf<Properties, String>(SupportedPropertyTypes.of().stringValue("") to "new_item_id")
        val propertyValueTranslations = mapOf<Properties, String>(SupportedPropertyTypes.of().stringValue("theId") to "theNewId")
        val logger = EventMapper(eventTranslations, propertyTranslations, propertyValueTranslations, logger = recorder)

        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("theId"))
        val copy = event.copy()
        event.log(logger)
        assertThat(recorder.events.size).isEqualTo(1)
        assertThat(recorder.events[0].getEventString()).isEqualTo("Start_App")
        assertThat(recorder.events[0].toProperties()["new_item_id"]).isEqualTo("theNewId")

        assertThat(event).isEqualTo(copy)
    }

    @Test
    fun testIdentifyTranslations() {
        val propertyValueTranslations = mapOf<Properties, String>(
                SupportedPropertyTypes.of().stringValue("superDevice") to "lousyDevice")
        val propertyTranslations = mapOf<Properties, String>(
                SupportedPropertyTypes.of().booleanValue(false) to "Zedge_user")
        val properties = SupportedPropertyTypes.of()
                .stringValue("superDevice")
                .booleanValue(true)
        EventMapper(
                propertyMappings = propertyTranslations,
                propertyValueMappings = propertyValueTranslations,
                logger = recorder
        ).identifyUser(properties)
        val result = recorder.userProperties[0].toProperties()
        assertThat(result.has("boolean")).isFalse()
        assertThat(result["Zedge_user"]).isEqualTo(true)
        assertThat(result["string"]).isEqualTo("lousyDevice")
    }

    @Test
    fun testValuesAsBookTitle() {
        val logger = EventMapper(keyMapping = BOOK_TITLE, enumMapping = BOOK_TITLE, logger = recorder)
        TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.with(SupportedPropertyTypes.of()
                .stringToIntMap(mapOf("notification_sound" to 3))
                .generalEnum(GeneralEnum.NAME_WITH_UNDERSCORE)).log(logger)
        assertThat(recorder.events[0].getEventString()).isEqualTo("Lose Events From Queue Limit")
        val properties = recorder.events[0].toProperties()
        assertThat(properties.getJSONArray("String To Int Values")[0]).isEqualTo(3)
        assertThat(properties["General Enum"]).isEqualTo("Name With Underscore")
    }

    @Test
    fun testNonEnumStringsAreLeftIntact() {
        val expected = "my exact_qUery"
        val logger = EventMapper(keyMapping = BOOK_TITLE, enumMapping = BOOK_TITLE, logger = recorder)
        TestEvent.RESUME_APP.with(SupportedPropertyTypes.of()
                .stringValue(expected)
                .thriftEnum(ThriftEnum.SECOND)).log(logger)
        val properties = recorder.events[0].toProperties()
        assertThat(properties["String"]).isEqualTo(expected)
        assertThat(properties["Thrift Enum"]).isEqualTo("Second")
    }
}
